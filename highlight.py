from pygments import highlight
from pygments.lexers.c_cpp import CppLexer
# from pygments.lexers.shell import BashSessionLexer
from pygments.formatters import HtmlFormatter
import lxml.html as html
import lxml.html.clean as clean
from html import unescape

cpplexer = CppLexer(stripall=True)
# bashlexer = BashSessionLexer(stripall=True)
# formatter = HtmlFormatter(linenos=False, cssclass="calibre24",
#                           style="monokailight") # default
cleaner = clean.Cleaner(allow_tags=['br'], remove_unknown_tags=False)


def extract(tag):
    return unescape(html.etree.tostring(cleaner.clean_html(tag))
                    .decode("utf-8")
                    .replace("\n", " ")
                    .replace('<br class="calibre6"/>', "\n")[5:-6])


def pygmentitfy(file):
    tree = html.parse("original/" + file)
    for p in tree.xpath("//p[tt[span[@class='calibre24']]]"):
        bq = p.getparent()
        text = highlight(extract(p), cpplexer, formatter)
        el = html.fromstring(text)
        bq.remove(p)
        bq.append(el)
        # for p in tree.xpath("//span[@class='calibre5']"):
        #     text = highlight(extract(p), bashlexer, formatter)
    with open("public/" + file, "wb") as f:
        f.write(html.tostring(tree, pretty_print=True))
    return True


from multiprocessing import Pool
from os import listdir, system
from sys import argv

system("rm -r original/*.html original/images &"
       "unzip {zip} -d original".format(zip=argv[1]))

formatter = HtmlFormatter(linenos=False, cssclass="calibre24",
                      style=argv[2])

with Pool(5) as p:
    res = p.imap(pygmentitfy, (f for f in listdir("original")
                               if f.endswith(".html")))
    for i in res:
        print(i)

system("pygmentize -S {style} -f html > pygment.css".format(style=argv[2]))
system("cp *.css original; cp *.css public;"
       "cp -avr original/images public")
